#include "types.h"
#include "stat.h"
#include "user.h"

#define PGSIZE 4096

int main(int argc, char const *argv[])
{
    //testing part 1
    int arr[15];
    char buf[10];
    for(int i = 0; i < 10; i++)
    {
        arr[i] =(int) sbrk(PGSIZE);
        if(arr[i] == -1)
        {
            printf(1,"something with sbrk goes wrong\n");
            return -1;
        }
        printf(1,"press any key\n");
        gets(buf, 10);
    }
    printf(1,"before calling last sbrk\n");
    if((int) sbrk(-4096) == -1)
    {
        printf(1,"something with sbrk goes wrong\n");
        return -1;
    }
    printf(1,"press any key2\n");
    gets(buf, 10);
    exit();
    return 0;
}
